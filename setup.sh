#!/bin/bash

if which python3.8; then
    PY=$(which python3.8)
elif which python3; then
    PY=$(which python3)
elif which python; then
    PY=$(which python)
fi

VER=$($PY -c 'import sys; print("".join(map(str, sys.version_info[:2])))')
if [ "$VER" -lt "38" ]; then
    echo "Install Python 3.8 or better"
    exit 1
fi

$PY -m pip install -U -r requirements.txt