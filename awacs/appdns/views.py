from django.contrib import messages
from django.shortcuts import render
from django.views import generic
from django.views.generic.base import TemplateView

import dns.name
import dns.resolver

class IndexView(TemplateView):
    template_name = 'dns/index.html'
    
    def get_context_data(self, **kwargs):
        resolver = dns.resolver.Resolver(configure=False)
        resolver.nameservers = ['8.8.8.8']
        answer = resolver.query('amazon.com', 'NS')
        msg = 'The nameservers are:<br>\n'
        for rr in answer:
            msg += f"{rr.target}<br>\n"
        return {'message':msg}

