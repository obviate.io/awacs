from django.apps import AppConfig


class WorldtimeConfig(AppConfig):
    name = "worldtime"
