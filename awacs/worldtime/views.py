from django.contrib import messages
from django.shortcuts import render
from django.views import generic
from django.views.generic.base import TemplateView

# Create your views here.

class IndexView(TemplateView):
    template_name = 'worldtime/index.html'
    