from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("dns/",include('appdns.urls')),
    path("time/",include('worldtime.urls')),
    path("",include('index.urls')),
    path("admin/", admin.site.urls),
]
