from django.contrib import messages
from django.shortcuts import render
from django.views import generic
from django.views.generic.base import TemplateView

# Create your views here.

class IndexView(TemplateView):
    template_name = 'index/index.html'
    
    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     messages.info(self.request, "hello http://example.com")
    #     return context